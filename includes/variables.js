const env = require('../env');

const Variables = {
	jwtTokenSecretKey: '8ri73n3duc47i0nC3n73r', // Aditya Sarma
	adminPortalBaseUrl: env.adminPortalBaseUrl,
	gymOwnerPortalUrl: env.gymOwnerPortalUrl,
	sitePortalUrl: env.sitePortalUrl,
	backendUrl: env.backendUrl,
	uploadPath: env.backendUrl + "images/uploads/",
	ccAvenueWorkingKey: 'ECD1DFA8F917089545A2EE2B0889751B',
	ccAvenueAccessCode: 'AVYB90HB80CH66BYHC',
	ccAvenueMerchantId: '244320',
	cmsDataPerPage : 10,
	mcourseDataPerPage : 10,
	tcourseDataPerPage : 10,
	applicationlistingsDataPerPage : 10,
	siteSettingDataPerPage : 10,
	gymOwnerListingDataPerPage  : 20,
	gymBranchListingDataPerPage  : 20,
	siteReviewListingDataPerPage  : 20,
	userApplicationListingDataPerPage  : 20,
	sitePortalgymListingDataPerPage  : 20,
	sitePortalCustomerFitcoinListingDataPerPage  : 20,
	sitePortalCustomerTransactionListingDataPerPage  : 20,
	vendorPortalAccountManagementListingDataPerPage  : 20,
	gymOwnerFeedbackToAdminListingsDataPerPage  : 50,
	adminFeedbackToGymOwnerListingsDataPerPage  : 50,
	adminAccountsManagementListingsDataPerPage  : 50,
	orderCancellationRequestDataPerPage  : 50,
	testimonialListingsDataPerPage : 20,
	adminPortalCustomerListingsDataPerPage : 50,
	sitePortalOrderHistoryUpcomingListingDataPerPage : 50,
	sitePortalOrderHistoryCompletedListingDataPerPage : 50,
	sitePortalOrderHistoryCancelledListingDataPerPage : 50,
	faqListingsDataPerPage : 20,
	fitCoinPurchasePackage : 50,
	orderHistoryListings : 50,
	adminCompetitionsListingPage : 50,
	adminCompetitionApplicationsListingPage : 50,
	unAuthorizedAccessMsg: 'Unauthorized access!',
	// SMTP_HOST: 'smtp.gmail.com',
	// SMTP_PORT: 587,
	// SMTP_SECURE: false,
	// SMTP_USERNAME: 'saptarshidev.2020@gmail.com',
	// SMTP_PASSWORD: 'S@221093b',
	// mailSendAdminName: 'Test Admin',
	// mainSiteUrl: 'http://wgi-aws-php-1241812835.ap-south-1.elb.amazonaws.com/ras-zone/',
	// adminEmailId: 'saptarshidev.2020@gmail.com',
	SMTP_HOST: 'mail.quality-web-programming.com',
	SMTP_PORT: 587,
	SMTP_SECURE: false,
	SMTP_USERNAME: 'smtp@quality-web-programming.com',
	SMTP_PASSWORD: 'nopass@123',
	mailSendAdminName: 'RAS ZONE',
	mainSiteUrl: 'http://wgi-aws-php-1241812835.ap-south-1.elb.amazonaws.com/ras-zone/',
	adminEmailId: 'saptarshidev.2020@gmail.com',
	escapeChar: (str) => str.replace(/'/g, "\\'"),

	vendorInvoiceMailSubject: '[_COMPANY_NAME_]: Your Invoice Details',
	vendorInvoiceMailBody: `
		<p>
			Hello [_USER_NAME_],
			<br><br>
			Please check your invoice details.
			
			<br><br><br><br>
			Thanks & Regards,
			<br>
			[_COMPANY_NAME_]
		</p>
	`,

	vendorBankDetailsSubmitToAdminMailSubject: '[_COMPANY_NAME_]: verification request form vendor',
	vendorBankDetailsSubmitToAdminMailBody: `
		<p>
			Hello [_USER_NAME_],
			<br><br>
			Please check vendor details: <a href="[_VENDOR_DETAILS_URL_]" target="_blank">Click here</a>
			
			<br><br><br><br>
			Thanks & Regards,
			<br>
			[_COMPANY_NAME_]
		</p>
	`,
	customerInvoiceMailSubject: '[_COMPANY_NAME_]: Your Invoice Details',
	customerInvoiceMailBody: `
		<p>
			Hello [_USER_NAME_],
			<br><br>
			Please check your invoice details.
			
			<br><br><br><br>
			Thanks & Regards,
			<br>
			[_COMPANY_NAME_]
		</p>
	`,
	adminVerifiedVendorMailSubject: '[_COMPANY_NAME_]: Congrats! Your account has been activated.',
	adminVerifiedVendorMailBody: `
		<p>
			Congrats [_USER_NAME_]!
			<br><br>
			Your account has been activated. <a href="[_VENDOR_LOGIN_PAGE_LINK_]" target="_blank">Click here</a> to login now by using your registered email and password.

			<br><br><br><br>
			Thanks & Regards,
			<br>
			[_COMPANY_NAME_]
		</p>
	`,
	gymOwnerForgetPasswordMailBody: `
		<p>
			Hello [_USER_NAME_],
			<br><br>
			Please click the link below to reset your password. Or you can directly copy and paste the url into your browser.
			<br><br>

			<a href="[_RESET_PASSWORD_LINK_]" target="_blank">[_RESET_PASSWORD_LINK_]</a>
			<br><br><br><br>
			Thanks & Regards,
			<br>
			[_COMPANY_NAME_]
		</p>
	`,
	gymOwnerForgetPasswordMailSubject: '[_COMPANY_NAME_]: Reset your password',
	
	customerForgetPasswordMailBody: `
		<p>
			Hello [_USER_NAME_],
			<br><br>
			Please click the link below to reset your password. Or you can directly copy and paste the url into your browser.
			<br><br>

			<a href="[_RESET_PASSWORD_LINK_]" target="_blank">[_RESET_PASSWORD_LINK_]</a>
			<br><br><br><br>
			Thanks & Regards,
			<br>
			[_COMPANY_NAME_]
		</p>
	`,
	customerForgetPasswordMailSubject: '[_COMPANY_NAME_]: Reset your password',

	sitePortalGymDetailsAppliedMailSubject: '[_COMPANY_NAME_]: Thank you for apply',

	sitePortalGymDetailsAppliedMailBody: `
		<p>
			Hello [_USER_NAME_],
			<br><br>
			Thank you for your application. Your application details are:
			<br><br>

			Application Id: [_APPLICATION_ID_]
			<br>
			Package Id: [_PACKAGE_ID_]
			<br>
			Package Details: [_NUMBER_OF_UNITS_] [_NAME_OF_UNIT_] [_NUMBER_OF_SESSIONS_] Session(s)
			<br>
			Package Price: [_PACKAGE_PRICE_] INR or [_FITCOINS_AMOUNT_] Fitcoins
			<br><br>

			Your application is under process. Once it is approved, you will receive another mail from respective vendor.
			<br><br><br><br>
			Thanks & Regards,
			<br>
			[_COMPANY_NAME_]
		</p>
	`,

	vendorPortalGymDetailsApplicationApprovedMailSubject: '[_COMPANY_NAME_]: Congrats! Your application has been approved',

	vendorPortalGymDetailsApplicationApprovedMailBody: `
		<p>
			Congrats [_USER_NAME_]!
			<br><br>
			Your application, #[_APPLICATION_ID_], has been approved. Please contact concerned branch accordingly within 7 Days of receiving this mail.
			<br><br>

			There you need to scan a QR Code to make your attendance. On first day scan you need to make the payment.
			<br><br>

			Application Id: [_APPLICATION_ID_]
			<br>
			Package Id: [_PACKAGE_ID_]
			<br>
			Package Details: [_NUMBER_OF_UNITS_] [_NAME_OF_UNIT_] [_NUMBER_OF_SESSIONS_] Session(s)
			<br>
			Package Price: [_PACKAGE_PRICE_] INR or [_FITCOINS_AMOUNT_] Fitcoins

			<br><br><br><br>
			Thanks & Regards,
			<br>
			[_COMPANY_NAME_]
		</p>
	`,

	sitePortalGymDetailsEnquiryMailSubject: '[_COMPANY_NAME_]: Enquiry submitted from site',

	sitePortalGymDetailsEnquiryMailBody: `
		<p>
			Hello [_RECEIVER_NAME_],
			<br><br>
			
			An enquiry has been submitted from site. Here is the description given below:
			<br><br>

			<strong>Enquiry Id:</strong> [_ENQUIRY_ID_]
			<br>
			<strong>Sender Name:</strong> [_SENDER_NAME_]
			<br>
			<strong>Sender Id:</strong> [_SENDER_ID_]
			<br>
			<strong>Sender Email:</strong> [_SENDER_EMAIL_]
			<br>
			<strong>Gym Name:</strong> [_GYM_NAME_]
			<br>
			<strong>Gym Id:</strong> [_GYM_ID_]
			<br>
			<strong>Enquiry Subject:</strong> [_ENQUIRY_SUBJECT_]
			<br>
			<strong>Enquiry Message:</strong> [_ENQUIRY_MESSAGE]
			
			<br><br><br><br>
			Thanks & Regards,
			<br>
			[_COMPANY_NAME_]
		</p>
	`,

	userRegistrationActive: '[_COMPANY_NAME_]: Account Activation',

	userRegistration: `
	<p>
		Hello [_USER_NAME_],
		<br><br>
		Thank you for registering with us. Please click the link below to activate your email.
		<br><br>

		<a href="[_USER_ACTIVATION_LINK_]" target="_blank">[_USER_ACTIVATION_LINK_]</a>
		<br><br><br><br>
		Thanks & Regards,
		<br>
		[_COMPANY_NAME_]
	</p>
`,
	// Mobile App Response Messages Starts //
	
	// Mobile App Response Messages Ends //
};

module.exports = Variables;