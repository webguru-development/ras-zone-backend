const { Pool, Client } = require('pg'),
       dbConfigs = require('../configs/db-configs');

      conn = new Pool({
        host: dbConfigs.DB_HOST,
        port: dbConfigs.DB_PORT,
        user: dbConfigs.DB_USERNAME,
        password: dbConfigs.DB_PASSWORD,
        database: dbConfigs.DB_NAME
      });

module.exports = conn;