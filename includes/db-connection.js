let mysql = require('mysql'),
    dbConfigs = require('../configs/db-configs');

// const DbConnection = mysql.createPool({
//   connectionLimit : 10,
//   host: dbConfigs.DB_HOST,
//   port: dbConfigs.DB_PORT,
//   user: dbConfigs.DB_USERNAME,
//   password: dbConfigs.DB_PASSWORD,
//   database: dbConfigs.DB_NAME
// });

const DbConnection = mysql.createConnection({
  host: dbConfigs.DB_HOST,
  port: dbConfigs.DB_PORT,
  user: dbConfigs.DB_USERNAME,
  password: dbConfigs.DB_PASSWORD,
  database: dbConfigs.DB_NAME
});

module.exports = DbConnection;