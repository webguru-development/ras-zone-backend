let queryBuilder = require('./query-builder');
    conn = require('../includes/db-connection-pg');
    dbConfigs = require('../configs/db-configs');

let QueryExecute = {
  executeQuery: (spName, paramsArr) => {
    return new Promise((resolve, reject) => {

      let sqlQuery = queryBuilder.buildQuery(spName, paramsArr);
      console.log(sqlQuery);
      conn.query(sqlQuery, (err, result) => {

        if (err) {
          resolve({
            'status': 201,
            'message': err
          });
        };
        // conn.end();
        try {
        	resolve(result['rows'][0][`${dbConfigs.DB_PREFIX}sp_${spName}`]);
        }
        catch(err) {
        	resolve({'error': JSON.stringify(err)});
        }
      });

    });
  }
};

module.exports = QueryExecute;