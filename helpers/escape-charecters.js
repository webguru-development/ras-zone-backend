let escapeChars = (str) => {
  return str.toString().replace('\t', '').split('\r\n')[0];
};

module.exports = escapeChars;