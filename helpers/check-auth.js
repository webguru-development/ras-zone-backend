const jwt = require('jsonwebtoken-refresh'),
      variables = require('../includes/variables');

module.exports = (req, res, next) => {
  try {
    let decoded = jwt.verify(req.headers.authorization, variables.jwtTokenSecretKey);
    var originalDecoded = jwt.decode(req.headers.authorization, {complete: true});
    var refreshed = jwt.refresh(originalDecoded, 3600, variables.jwtTokenSecretKey);
    
    // console.log(JSON.stringify(originalDecoded));
    // console.log(JSON.stringify(jwt.decode(refreshed, {complete: true})));
    req.refreshedToken = refreshed;
    next();
  } catch (error) {
    console.log(error);
    return res.status(401).json({
      message: variables.unAuthorizedAccessMsg
    });
  }
};