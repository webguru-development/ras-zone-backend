const nodemailer = require("nodemailer"),
      variables = require("../includes/variables");

// sample attachment array

let sendMail = {
  send: (senderName, senderEmail, receiverEmails, replyTo, mailSubject, mailBodyHtml, attachments = []) => {
    return new Promise((resolve, reject) => {
      // create reusable transporter object using the default SMTP transport
      let transporter = nodemailer.createTransport({
        host: variables.SMTP_HOST,
        port: variables.SMTP_PORT,
        secure: variables.SMTP_SECURE, // true for 465, false for other ports
        auth: {
          user: variables.SMTP_USERNAME, // generated ethereal user
          pass: variables.SMTP_PASSWORD // generated ethereal password
        },
        tls: {
          rejectUnauthorized: false
      }
      });

      // send mail with defined transport object
      let info = transporter.sendMail({
        from: '"'+ senderName + '" <' + senderEmail + '>', // sender address
        to: receiverEmails, // list of receivers
        replyTo: replyTo,
        subject: mailSubject, // Subject line
        // text: "Node JS mail functionality checking.", // plain text body
        html: mailBodyHtml, // html body
        attachments: attachments.length > 0 ? attachments : false  
      });

      resolve(1);
    });
  }
}

module.exports = sendMail;