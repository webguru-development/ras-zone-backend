const pdf = require('html-pdf');

let GenerateInvoice = (htmlContent, fileNameToSaveWithPath) => {
  return new Promise((resolve, reject) => {
    pdf.create(htmlContent, {
      // "format": "A4"
      "height": "1178px",
      "width": "833px"
    }).toFile(fileNameToSaveWithPath, (err, res) => {
      if (err) return console.log(err);
      console.log(res); // { filename: '/app/businesscard.pdf' }
      resolve(1);
    });
  });
};

module.exports = GenerateInvoice;
