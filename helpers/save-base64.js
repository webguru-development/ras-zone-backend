let SaveBase64 = (imageDataToSave, imageNameWithPathToSave, imageExtension = 'jpeg') => {
  /**
  *@DESC IMAGE upload from base64 
  **/
  return new Promise((resolve, reject) => {
	  let regExp = new RegExp(`/^data:image\/${imageExtension};base64,/`),
	    	base64Data = imageDataToSave.replace(`data:image/${imageExtension};base64,`, "");

	  require("fs").writeFile(__dirname + '/../' + imageNameWithPathToSave, base64Data, {encoding: 'base64'}, function(err) {
	    if(err) {
	    	console.log(err);
	    	resolve(0);
	    }

	    resolve(1);
	  });
	});
};

module.exports = SaveBase64;