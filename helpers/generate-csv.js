const fs = require('fs');
const os = require('os');

let csv = {
  create: (resFilePath, data) => {
    return new Promise((resolve, reject) => {    
      let output = []; // holds all rows of data
      
      for(let obj of data){
        let objKeys = Object.keys(obj),
            row = [];
        for(let key of objKeys){
          // row.push(obj[key]);
          if(key != "product_tax_id" ){
            row.push(obj[key]);
          }
        }
        output.push(row);
      }      
      fs.writeFileSync(resFilePath, output.join(os.EOL));      
      resolve(1);
    });
  }
}

module.exports = csv;