let dbConfigs = require('../configs/db-configs');

let QueryBuilder = {
  buildQuery: (spName, paramsArr) => {
    let dbSchemaName = dbConfigs.DB_SCHEMA;
    if(spName.indexOf('mobile_portal_') >= 0) {
      dbSchemaName = 'mobile_schema';
    }
    let query = `SELECT * FROM ${dbSchemaName}.${dbConfigs.DB_PREFIX}sp_${spName}(`;

    for(let i in paramsArr) {
      query += paramsArr[i];
      if(i < paramsArr.length - 1) {
        query += ',';
      }
    }

    query += ');';

    return query;
  }
};

module.exports = QueryBuilder;