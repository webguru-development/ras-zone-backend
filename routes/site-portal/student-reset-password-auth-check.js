let express = require('express'),
    router = express.Router(),
    queryExecute = require('../../helpers/query-execute'),
    validator = require('../../helpers/validators');
 
/* Customer Reset Password Authentication Check. */
router.post('/', function(req, res, next) {
  let reqBody = req.body;

  if(!validator.isRequired(reqBody.request_token)) {
    res.send({'status': 201});
  }
  
  queryExecute.executeQuery('site_portal_user_reset_password_auth', [
    "'"+reqBody.request_token+"'",
  ]).then(resp => {
    res.send(resp);
  });
});

module.exports = router;
