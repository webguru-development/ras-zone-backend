let express = require('express'),
    router = express.Router(),
    queryExecute = require('../../helpers/query-execute'),
    validator = require('../../helpers/validators');

/* Post Site Portal Questions Page Data. */
router.post('/', function (req, res, next) {
  let reqBody = req.body,
      user_id = !!validator.isRequired(reqBody.user_id) ? "'" + reqBody.user_id + "'" : null,
      access_token = !!validator.isRequired(reqBody.access_token) ? "'" + reqBody.access_token + "'" : null,
      exam_id = !!validator.isRequired(reqBody.exam_id) ? "'" + reqBody.exam_id + "'" : null,
      questionwiseanswerdata = !!validator.isRequired(reqBody.questionwiseanswerdata) ? "'" + reqBody.questionwiseanswerdata + "'" : null;

  queryExecute.executeQuery('site_portal_student_question_answer_submit', [
        user_id,
        access_token,
        exam_id,
        reqBody.isfinalsubmit,
        questionwiseanswerdata
  ]).then(resp => {
  console.log(resp);
    res.send(resp);
  });
});

module.exports = router;
