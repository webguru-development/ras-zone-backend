let express = require('express'),
    router = express.Router(),
    queryExecute = require('../../helpers/query-execute'),
    validator = require('../../helpers/validators');

/* Post Site Portal Home Page Data. */
router.post('/', function (req, res, next) {
  let reqBody = req.body,
      user_id = !!validator.isRequired(reqBody.user_id) ? "'" + reqBody.user_id + "'" : null,
      access_token = !!validator.isRequired(reqBody.access_token) ? "'" + reqBody.access_token + "'" : null;

  queryExecute.executeQuery('site_portal_home_page_data', [
        user_id,
        access_token,
  ]).then(resp => {
    res.send(resp);
  });
});

module.exports = router;
