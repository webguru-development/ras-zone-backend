let express = require('express'),
    router = express.Router(),
    queryExecute = require('../../helpers/query-execute'),
    validator = require('../../helpers/validators');

/* Post Site Portal Paper Packages Page Data. */
router.post('/', function (req, res, next) {
  let reqBody = req.body,
      user_id = !!validator.isRequired(reqBody.user_id) ? "'" + reqBody.user_id + "'" : null,
      access_token = !!validator.isRequired(reqBody.access_token) ? "'" + reqBody.access_token + "'" : null,
      search_text = !!validator.isRequired(reqBody.search_text) ? "'" + reqBody.search_text + "'" : null,
      sort_by = !!validator.isRequired(reqBody.sort_by) ? "'" + reqBody.sort_by + "'" : null,
      page_no = !!validator.isRequired(reqBody.page_no) ? "'" + reqBody.page_no + "'" : null;
  queryExecute.executeQuery('site_portal_student_purchase_history_page_data', [
        user_id,
        access_token,
        search_text,
        sort_by,
        page_no
  ]).then(resp => {
  console.log(resp);
    res.send(resp);
  });
});

module.exports = router;
