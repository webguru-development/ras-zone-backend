let express = require('express'),
    router = express.Router(),
    queryExecute = require('../../helpers/query-execute'),
    validator = require('../../helpers/validators');
 
/* Customer Profile Pic Data. */
router.post('/', function(req, res, next) {
  let reqBody = req.body;

  if(!validator.isRequired(reqBody.user_id) || !validator.isRequired(reqBody.access_token)) {
    res.send({'status': 201});
  }
  
  queryExecute.executeQuery('rz_sp_site_portal_student_profile_pic_data', [
    "'"+reqBody.user_id+"'",
    "'"+reqBody.access_token+"'",
  ]).then(resp => {
    res.send(resp);
  });
});

module.exports = router;
