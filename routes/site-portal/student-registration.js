let express = require('express'),
  router = express.Router(),
  queryExecute = require('../../helpers/query-execute'),
  validators = require('../../helpers/validators'),  
  variables = require('../../includes/variables'),
  sendMail = require('../../helpers/send-mail'),
  fs = require('fs');

/* POST Site Portal Customer Registration. */
router.post('/', function (req, res, next) {
  let reqBody = req.body;

  // if(!validators.isRequired(reqBody.user_title_id) || !validators.isNotNull(reqBody.user_title_id)) {
  //   res.send({ status: 201, errCode: 1 });
  // }
  // else 
  if(!validators.isRequired(reqBody.user_full_name) || !validators.isNotNull(reqBody.user_full_name) || !validators.isAlphabetsOnly(reqBody.user_full_name)) {
      res.send({ status: 201, errCode: 2 });
  }
  else if(!validators.isRequired(reqBody.user_email) || !validators.isNotNull(reqBody.user_email) || !validators.isEmail(reqBody.user_email)) {
    res.send({ status: 201, errCode: 3 });
  }  
  else if(!validators.isRequired(reqBody.user_password) || !validators.isNotNull(reqBody.user_password)) {
    res.send({ status: 201, errCode: 4 });
  }
  else if(reqBody.user_password != reqBody.user_confirm_password) {
    res.send({ status: 201, errCode: 5 });
  }
  // else if(!validators.isRequired(reqBody.contact_number) || !validators.isNotNull(reqBody.contact_number) ||     !validators.isValidMobileNumber(reqBody.contact_number)) {
  //   res.send({ status: 201, errCode: 6 });
  // }
  // else if(!validators.isRequired(reqBody.address_street) || !validators.isNotNull(reqBody.address_street)) {
  //   res.send({ status: 201, errCode: 7 });
  // } 
  // else if(!validators.isRequired(reqBody.address_city) || !validators.isNotNull(reqBody.address_city)) {
  //   res.send({ status: 201, errCode: 8 });
  // } 
  // else if(!validators.isRequired(reqBody.address_state) || !validators.isNotNull(reqBody.address_state)) {
  //   res.send({ status: 201, errCode: 9 });
  // }
  // else if(!validators.isRequired(reqBody.address_country) || !validators.isNotNull(reqBody.address_country)) {
  //   res.send({ status: 201, errCode: 9 });
  // }  
  // else if(!validators.isRequired(reqBody.address_pin) || !validators.isNotNull(reqBody.address_pin)) {
  //   res.send({ status: 201, errCode: 10 });
  // }

  queryExecute.executeQuery('site_portal_student_registration', [
    "'" + reqBody.user_title_id + "'",
    "'" + reqBody.user_full_name + "'",
    "'" + reqBody.user_email + "'",
    "'" + reqBody.user_password + "'",
    "'" + reqBody.contact_number + "'",
    "'" + reqBody.address_street + "'",
    "'" + reqBody.address_city + "'",
    "'" + reqBody.address_state + "'",
    "'" + reqBody.address_country + "'",
          reqBody.address_pin
  ]).then(resp => {
    if (resp.status == 200) {
      console.log(resp);
      let activationToken = resp.data.activation_token;
      
      fs.mkdir('public/images/uploads/students/' + resp.data.user_id + '/', (err) => {
        if (err) {
          console.log(err);
        }    
      });

      fs.copyFile('public/images/dummy-user.png', 'public/images/uploads/students/' + resp.data.user_id + '/' + resp.data.user_id + 'c-profile-pic.jpeg', (err) => {
        if (err) throw err;
        console.log('source.txt was copied to destination.txt');
      });

      fs.copyFile('public/images/dummy-user.png', 'public/images/uploads/students/' + resp.data.user_id + '/' + resp.data.user_id + 's-profile-pic.jpeg', (err) => {
        if (err) throw err;
        console.log('source.txt was copied to destination.txt');
      });

      userName = reqBody.user_full_name,
      mailBody = variables.userRegistration
                .replace(/\[_USER_NAME_\]/g, userName)
                .replace(/\[_USER_ACTIVATION_LINK_\]/g, variables.sitePortalUrl + 'account-activate/' + activationToken)
                .replace(/\[_COMPANY_NAME_\]/g, variables.mailSendAdminName),
      mailSubject = variables.userRegistrationActive.replace(/\[_COMPANY_NAME_\]/g, variables.mailSendAdminName);

      // Send Mail To Site User
      sendMail.send(variables.mailSendAdminName, variables.SMTP_USERNAME, reqBody.user_email, variables.SMTP_USERNAME, mailSubject, mailBody).then(result1 => {
        res.send({
          status: resp.status
        });
      });
    }
    else {
      res.send(resp);
    }
  });
});

module.exports = router;
