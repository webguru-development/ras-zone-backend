let express = require('express'),
    router = express.Router(),
    queryExecute = require('../../helpers/query-execute'),
    sendMail = require('../../helpers/send-mail'),
    variables = require('../../includes/variables')
    validator = require('../../helpers/validators');

/* POST Customer Forget Password. */
router.post('/', function(req, res, next) {
  let reqBody = req.body;

  if( 
    !validator.isNotNull(reqBody.user_email) || 
    !validator.isEmail(reqBody.user_email)
  ) {
    res.send({'status': 201});
  }

  queryExecute.executeQuery('site_portal_user_forget_password', [
    "'"+reqBody.user_email+"'"
  ]).then(resp => {
    if(resp.status == 200) {
      // Send Mail To Site User
      let mailBody = variables.customerForgetPasswordMailBody
                .replace(/\[_USER_NAME_\]/g, resp.data.user_name)
                .replace(/\[_RESET_PASSWORD_LINK_\]/g, variables.sitePortalUrl + 'student-reset-password/' + resp.data.request_token)
                .replace(/\[_COMPANY_NAME_\]/g, variables.mailSendAdminName),
      mailSubject = variables.customerForgetPasswordMailSubject.replace(/\[_COMPANY_NAME_\]/g, variables.mailSendAdminName);

      sendMail.send(variables.mailSendAdminName, variables.SMTP_USERNAME, reqBody.user_email, variables.SMTP_USERNAME, mailSubject, mailBody).then(result1 => {
        res.send({
          status: resp.status
        });
      });
    }
    else {
      res.send(resp);
    }
  });
});

module.exports = router;
