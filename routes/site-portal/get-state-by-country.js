let express = require('express'),
    router = express.Router(),
    queryExecute = require('../../helpers/query-execute'),
    validator = require('../../helpers/validators');
 
/* Customer Reset Password Authentication Check. */
router.post('/', function(req, res, next) {
  let reqBody = req.body;

  if(!validator.isRequired(reqBody.country_id)) {
    res.send({'status': 201});
  }
  
  queryExecute.executeQuery('site_portal_country_wise_states_listing', [
    "'"+reqBody.country_id+"'",
  ]).then(resp => {
    res.send(resp);
  });
});

module.exports = router;
