-- FUNCTION: ras_zone_schema.rz_sp_site_portal_purchase_paper_packages(character varying, character varying, json, boolean)

-- DROP FUNCTION ras_zone_schema.rz_sp_site_portal_purchase_paper_packages(character varying, character varying, json, boolean);

CREATE OR REPLACE FUNCTION ras_zone_schema.rz_sp_site_portal_purchase_paper_packages(
	userkey character varying,
	accesstoken character varying,
	purchasepackages json,
	isallpackages boolean)
    RETURNS json
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
DECLARE userid INT;
DECLARE inserteduserpurchasepapersid INT;
DECLARE paperid INT;
DECLARE loggedindata JSON;
DECLARE i JSON;
DECLARE j JSON;
DECLARE totalpricesums NUMERIC := 0; 
BEGIN

	SELECT * FROM ras_zone_schema.rz_sp_site_portal_is_student_logged_in(userkey, accesstoken) INTO loggedindata;
	IF (SELECT loggedindata->>'status')::INT != 200 THEN
		RETURN (
			SELECT JSON_BUILD_OBJECT(
				'status', (loggedindata->>'status')::INT
			)
		);
	END IF;

	IF isallpackages != TRUE THEN
		FOR j IN SELECT * FROM json_array_elements(purchasepackages) LOOP
			-- SELECT id INTO paperid FROM ras_zone_schema.rz_tbl_master_papers WHERE paper_key = (j::JSON)->>'paper_id');
			-- totalpricesums := totalpricesums + (SELECT paper_price::NUMERIC FROM ras_zone_schema.rz_tbl_master_papers WHERE id = paperid;
			totalpricesums := totalpricesums + (SELECT paper_price::NUMERIC FROM ras_zone_schema.rz_tbl_master_papers WHERE id = (SELECT id FROM ras_zone_schema.rz_tbl_master_papers WHERE paper_key = (j::JSON)->>'paper_id'));
		END LOOP;
	ELSE		
		SELECT
			SUM (paper_price) INTO totalpricesums
		FROM
			ras_zone_schema.rz_tbl_master_papers;
	END IF;


	INSERT INTO ras_zone_schema.rz_tbl_trans_user_purchase_papers (
		user_id,
		total_price,
		created_at,
		created_by
	) VALUES (
		(loggedindata->>'user_id')::INT,
		totalpricesums,
		NOW(),
		(loggedindata->>'user_id')::INT		
	) RETURNING id INTO inserteduserpurchasepapersid;


	INSERT INTO ras_zone_schema.rz_tbl_trans_transaction_logs (
		purchase_id,
		total_price,
		bank_transaction_id,
		transaction_status,
		transaction_return_url,
		created_at,
		created_by
	) VALUES (
		inserteduserpurchasepapersid,
		totalpricesums,
		'1',
		FALSE,
		'transaction_return_url',		
		NOW(),
		(loggedindata->>'user_id')::INT		
	);


	FOR i IN SELECT * FROM json_array_elements(purchasepackages) LOOP
		INSERT INTO ras_zone_schema.rz_tbl_trans_purchased_paper_modules(
			purchase_id,
			paper_id,
			paper_price
		)
		VALUES
		(
			inserteduserpurchasepapersid,
			(SELECT id FROM ras_zone_schema.rz_tbl_master_papers WHERE paper_key = ((i::JSON)->>'paper_id')),			
			(SELECT paper_price::NUMERIC FROM ras_zone_schema.rz_tbl_master_papers WHERE id = (SELECT id FROM ras_zone_schema.rz_tbl_master_papers WHERE paper_key = (i::JSON)->>'paper_id'))
		);
	END LOOP;

	RETURN (
		SELECT JSON_BUILD_OBJECT(			
			'user_status', (loggedindata->>'status')::INT,
			'status', 200
		)
	);

	EXCEPTION WHEN OTHERS THEN
	RETURN (
		SELECT JSON_BUILD_OBJECT(
			'message', SQLERRM,
			'status', 402
		)
	);
END;
$BODY$;

ALTER FUNCTION ras_zone_schema.rz_sp_site_portal_purchase_paper_packages(character varying, character varying, json, boolean)
    OWNER TO ras_zone_user;



