let express = require('express'),
    router = express.Router(),
    queryExecute = require('../../helpers/query-execute'),
    validator = require('../../helpers/validators');

/* Post Site Portal Logout */
router.post('/', function (req, res, next) {
  let reqBody = req.body,
      userId = typeof reqBody.user_id !== 'undefined' && reqBody.user_id != '' ? "'" + reqBody.user_id + "'" : 'null',
      accessToken = typeof reqBody.access_token !== 'undefined' && reqBody.access_token != '' ? "'" + reqBody.access_token + "'" : 'null';

  // if(!validator.isRequired(reqBody.user_id) && !validator.isRequired(reqBody.access_token)){
  //   res.send({status:201})
  // }

  queryExecute.executeQuery('site_portal_user_logout', [
    userId,
    accessToken
  ]).then(resp => {
    res.send(resp);
  });
});

module.exports = router;
