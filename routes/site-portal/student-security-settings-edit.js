let express = require('express'),
    router = express.Router(),
    queryExecute = require('../../helpers/query-execute'),
    validator = require('../../helpers/validators'),
    variables = require('../../includes/variables');
    

/* get Security Settings Change. */
router.post('/', function(req, res, next) {
  let reqBody = req.body;

  if(!validator.isRequired(reqBody.user_id) || 
    !validator.isRequired(reqBody.access_token) ||
    !validator.isRequired(reqBody.old_password) ||
    !validator.isRequired(reqBody.new_password) ||
    !validator.isRequired(reqBody.confirm_password) ||
    reqBody.new_password != reqBody.confirm_password) {
    res.send({'status': 201});
  }
  
  queryExecute.executeQuery('site_portal_student_security_settings_edit', [
    "'"+reqBody.user_id+"'",
    "'"+reqBody.access_token+"'",
    "'"+reqBody.old_password+"'",
    "'"+reqBody.new_password+"'",
  ]).then(resp => {
    res.send(resp);
  });
});

module.exports = router;
