let express = require('express'),
  router = express.Router(),
  queryExecute = require('../../helpers/query-execute'),
  validators = require('../../helpers/validators'),
  sendMail = require('../../helpers/send-mail'),
  fs = require('fs');

/* Customer Reset Password. */
router.post('/', function (req, res, next) {
  let reqBody = req.body;
  
  if(!validators.isRequired(reqBody.new_password) || !validators.isNotNull(reqBody.new_password)) {
    res.send({ status: 201, errCode: 1 });
  }
  else if(reqBody.new_password != reqBody.confirm_password) {
    res.send({ status: 201, errCode: 2 });
  }
  else if(!validators.isRequired(reqBody.request_token) || !validators.isNotNull(reqBody.request_token)) {
    res.send({ status: 201, errCode: 1 });
  }

  queryExecute.executeQuery('site_portal_user_reset_password', [
    "'" + reqBody.request_token + "'",
    "'" + reqBody.new_password + "'"
  ]).then(resp => {
    res.send(resp);
  });
});

module.exports = router;
