let express = require('express'),
    router = express.Router(),
    queryExecute = require('../../helpers/query-execute'),
    validator = require('../../helpers/validators');

/* Post Site Portal Paper Packages Page Data. */
router.post('/', function (req, res, next) {
  let reqBody = req.body,
      user_id = !!validator.isRequired(reqBody.user_id) ? "'" + reqBody.user_id + "'" : null,
      access_token = !!validator.isRequired(reqBody.access_token) ? "'" + reqBody.access_token + "'" : null,
      purchase_packages = !!validator.isRequired(reqBody.purchase_packages) ? "'" + reqBody.purchase_packages + "'" : null,
      is_all_packages = !!validator.isRequired(reqBody.is_all_packages) ? "'" + reqBody.is_all_packages + "'" : null;
  console.log(reqBody);
  queryExecute.executeQuery('site_portal_purchase_paper_packages', [
        user_id,
        access_token,
        purchase_packages,
        is_all_packages
  ]).then(resp => {
  console.log(resp);
    res.send(resp);
  });
});

module.exports = router;
