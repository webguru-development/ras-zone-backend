let express = require('express'),
    router = express.Router(),
    queryExecute = require('../../helpers/query-execute'),
    variables = require('../../includes/variables'),
    validator = require('../../helpers/validators');

/* POST Site Portal Vendor Login. */
router.post('/', function(req, res, next) {
  let reqBody = req.body;

  if(
    !validator.isRequired(reqBody.user_id) || 
    !validator.isNotNull(reqBody.user_id) ||
    !validator.isRequired(reqBody.access_token) || 
    !validator.isNotNull(reqBody.access_token)
  ) {
    res.send({'status': 201});
  }

  queryExecute.executeQuery('site_portal_student_profile_settings_page_data', [
    "'"+reqBody.user_id+"'",
    "'"+reqBody.access_token+"'"
  ]).then(resp => {
    if(resp.status == 200) {      
      res.send(resp);
    }
    else {
      res.send(resp);
    }
  });
});

module.exports = router;
