let express = require('express'),
    router = express.Router(),
    queryExecute = require('../../helpers/query-execute'),
    saveBase64 = require('../../helpers/save-base64'),
    validator = require('../../helpers/validators');
 
/* Customer Profile Pic Edit. */
router.post('/', function(req, res, next) {
  let reqBody = req.body,
      profilePicData = JSON.parse(reqBody.profile_pic_data);

  if(
    !validator.isRequired(reqBody.user_id) || 
    !validator.isRequired(reqBody.access_token) ||
    !validator.isRequired(profilePicData.cropped_img)
  ) {
    res.send({'status': 201});
  }
  
  queryExecute.executeQuery('site_portal_student_profile_picture_edit', [
    "'"+reqBody.user_id+"'",
    "'"+reqBody.access_token+"'",
    "'"+JSON.stringify(profilePicData.crop_data)+"'"
  ]).then(resp => {
    if(resp.status == 200) {
      saveBase64(profilePicData.source_img, 'public/images/uploads/students/' + reqBody.user_id + '/' + reqBody.user_id + 's-profile-pic.jpeg', 'jpeg').then(result1 => {
        saveBase64(profilePicData.cropped_img, 'public/images/uploads/students/' + reqBody.user_id + '/' + reqBody.user_id + 'c-profile-pic.jpeg', 'jpeg').then(result2 => {
          res.send(resp);
        });
      });
    }
  });
});

module.exports = router;
