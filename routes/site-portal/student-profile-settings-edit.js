let express = require('express'),
  router = express.Router(),
  queryExecute = require('../../helpers/query-execute'),
  validators = require('../../helpers/validators');

/* POST Site Portal Customer Basic Settings Edit. */
router.post('/', function (req, res, next) {
  let reqBody = req.body;

  if (!validators.isRequired(reqBody.user_id) ||
    !validators.isRequired(reqBody.access_token) || 
    !validators.isRequired(reqBody.user_title_id)|| !validators.isRequired(reqBody.user_full_name) ||
    !validators.isNotNull(reqBody.user_full_name) || !validators.isAlphabetsOnly(reqBody.user_full_name)
    // !validators.isRequired(reqBody.address_street) || !validators.isNotNull(reqBody.address_street) || 
    // !validators.isRequired(reqBody.address_city) || !validators.isNotNull(reqBody.address_city) || 
    // !validators.isRequired(reqBody.address_state) || !validators.isNotNull(reqBody.address_state) || 
    // !validators.isRequired(reqBody.address_country) || !validators.isNotNull(reqBody.address_country) || 
    // !validators.isRequired(reqBody.address_pin) || !validators.isNotNull(reqBody.address_pin) || 
    // !validators.isRequired(reqBody.contact_number) || !validators.isNotNull(reqBody.contact_number) || !validators.isValidMobileNumber(reqBody.contact_number) 
  ) {
    res.send({ 'status': 201 });
  }
  queryExecute.executeQuery('site_portal_student_profile_settings_edit', [
    "'" + reqBody.user_id + "'",
    "'" + reqBody.access_token + "'",
    "'" + reqBody.user_title_id + "'",
    "'" + reqBody.user_full_name + "'",
    "'" + reqBody.contact_number + "'",
    "'" + reqBody.address_street + "'",
    "'" + reqBody.address_city + "'",
    "'" + reqBody.address_state + "'",
    "'" + reqBody.address_country + "'",
          reqBody.address_pin
  ]).then(resp => {
    res.send(resp);
  });
  
});

module.exports = router;
