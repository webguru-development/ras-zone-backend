const Env = {
  environment: 'development', // 'development' / 'beta' / 'production'
  local: {
    SERVER_PORT: 5000,
    adminPortalBaseUrl: "http://192.168.5.37:3000/#/",    
    sitePortalUrl: 'http://localhost:4300/',
    backendUrl: 'http://localhost:4310/'
  },
  development: {
    SERVER_PORT: 5000,
    adminPortalBaseUrl: "http://192.168.5.37:3000/#/",    
    sitePortalUrl: 'http://wgi-aws-php-1241812835.ap-south-1.elb.amazonaws.com/ras-zone/site-portal/',
    backendUrl: 'http://localhost:4310/'
  },
  beta: {
    SERVER_PORT: 5000,
    adminPortalBaseUrl: "https://www.abc.in/beta/admin-portal/#/",
    sitePortalUrl: 'http://wgi-aws-php-1241812835.ap-south-1.elb.amazonaws.com/ras-zone/site-portal/',
    backendUrl: 'https://www.abc.in:4310/'
  },
  production: {
    SERVER_PORT: 5000,
    adminPortalBaseUrl: "https://admin-portal.abc.in/#/",    
    sitePortalUrl: 'https://www.abc.in/',
    backendUrl: 'https://www.abc.in:8443/'
  }
};

module.exports = Env[Env.environment];