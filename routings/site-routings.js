var express = require('express');
var router = express.Router();
const componentBase = '../routes/site-portal';
let dbConfigs = require('../configs/db-configs');


router.use('/site-settings-data', require(componentBase + '/site-settings-data'));
router.use('/student-registration-page-data', require(componentBase + '/student-registration-get-page-data'));
router.use('/get-state-by-country', require(componentBase + '/get-state-by-country'));
router.use('/get-city-by-state', require(componentBase + '/get-city-by-state'));
router.use('/student-registration', require(componentBase + '/student-registration'));
router.use('/student-account-activation', require(componentBase + '/student-registration-link-activation'));
router.use('/student-auth-check', require(componentBase + '/student-auth-check'));
router.use('/student-login', require(componentBase + '/student-login'));
router.use('/student-forget-password', require(componentBase + '/student-forget-password'));
router.use('/student-reset-password-auth-check', require(componentBase + '/student-reset-password-auth-check'));
router.use('/student-reset-password', require(componentBase + '/student-reset-password'));
router.use('/student-logout', require(componentBase + '/logout'));
router.use('/student-profile-settings-page-data', require(componentBase + '/student-profile-settings-page-data'));
router.use('/student-profile-settings-edit', require(componentBase + '/student-profile-settings-edit'));
router.use('/student-profile-pic-data', require(componentBase + '/student-profile-pic-data'));
router.use('/student-profile-pic-edit', require(componentBase + '/student-profile-pic-edit'));
router.use('/student-security-settings-edit', require(componentBase + '/student-security-settings-edit'));
router.use('/get-paper-packages-page-data', require(componentBase + '/paper-packages-page-data'));
router.use('/get-purchase-history-page-data', require(componentBase + '/get-purchase-history-page-data'));
router.use('/get-exam-ongoing-page-data', require(componentBase + '/get-exam-ongoing-page-data'));
router.use('/student-purchase-paper', require(componentBase + '/student-purchase-paper'));
router.use('/student-exam-details-page-data', require(componentBase + '/get-exam-details-page-data'));
router.use('/student-exam-questions-page-data', require(componentBase + '/student-questions-page-data'));
router.use('/get-student-answer-submit-page-data', require(componentBase + '/student-answer-submit-data'));
router.use('/get-student-report-page-data', require(componentBase + '/student-report-data'));

module.exports = router;